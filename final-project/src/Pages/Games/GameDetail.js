import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { message } from "antd";
import axios from "axios";
import Back from "../../Public/img/back.png";

const GameDetail = () => {
  let { id } = useParams();
  const [data, setData] = useState(null);

  useEffect(() => {
    if (data === null) {
      axios
        .get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
        .then((res) => {
          setData(res.data);
        })
        .catch((err) => {
          message.error(
            "Tidak dapat mengambil Data, Mohon tunggu dan Periksa Koneksi Anda"
          );
        });
    }
  }, [data, setData, id]);

  return (
    <>
      <div className="section">
        {data !== null &&
          [data].map((value) => (
            <div style={{ display: "flex" }}>
              <img src={value.image_url} alt="image_game" width={300} />
              <div style={{ marginLeft: 60 }}>
                <h1 style={{ marginBottom: 10 }}>
                  {value.name} ( {value.release} )
                </h1>
                <h4 style={{ marginBottom: 10 }}>
                  <strong>Tipe :</strong> {value.genre}
                </h4>
                <div>
                  <p>
                    <strong>platform :</strong> {value.platform}
                  </p>
                  <p>
                    <strong>Single Player :</strong>  {value.singlePlayer === 1 ? "Yes" : "No" }
                  </p>
                  <p>
                    <strong>Multi Player :</strong> {value.multiPlayer === 1 ? "Yes" : "No" }
                  </p>
                </div>
              </div>
            </div>
          ))}
      </div>
      <Link to="/" style={{ color: "black", paddingLeft: "40px" }}>
        {" "}
        <img src={Back} alt="back" width={30} /> Back
      </Link>
    </>
  );
};

export default GameDetail