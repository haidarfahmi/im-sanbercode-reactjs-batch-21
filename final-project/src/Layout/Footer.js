import React from "react";
import { Layout } from "antd";
import Logo from "../Public/img/Linkedin.png";
import LogoInsta from "../Public/img/instagram.png";

const { Footer } = Layout;


const footer = () => {
  return (
    <Footer
      style={{
        display: "flex",
        justifyContent: "space-between",
        backgroundColor: "#001529",
        color: "white",
        position: "fixed",
        bottom: 0,
        width: "100%",
        height: '8%',
        zIndex: 0
      }}
    >
      <p>
        <a
          target="blank"
          href="https://www.linkedin.com/in/haidar-fahmi-2bb09b200/"
        >
          <img src={Logo} alt="linkedin" width={40} />
        </a>{" "}
        <a target="blank" href="https://www.instagram.com/haidarfahmii/">
          <img src={LogoInsta} alt="instagram"  width={40} />
        </a>{" "}
      </p>
      <p><strong>SanberCode ©2021 Haidar Fahmi</strong></p>
    </Footer>
  );
};

export default footer;
