import React, { Component } from 'react';

class Timer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			jam: new Date().toLocaleTimeString(),
			time: 101,
			showTimer: true
		};
	}

	componentDidMount() {
		if (this.props.start !== undefined) {
			this.setState({ jam: this.props.start });
			this.setState({ time: this.props.start });
		}
		this.timerID = setInterval(() => this.tick(), 1000);
	}

	tick() {
		this.setState({
			time: this.state.time - 1,
			jam: new Date().toLocaleTimeString()
		});
	}

	componentDidUpdate() {
		if (this.state.showTimer === true) {
			if (this.state.time < 1) {
				this.setState({
					showTimer: false
				});
			}
		}
	}

	render() {
		return (
			<div>
				{this.state.showTimer && (
					<div style={{ display: 'flex', justifyContent: 'space-around' }}>
						<h1>sekarang jam - {this.state.jam}</h1>
						<h1>hitung mundur: {this.state.time}</h1>
					</div>
				)}
			</div>
		);
	}
}

export default Timer;
