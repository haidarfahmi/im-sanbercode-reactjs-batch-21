import React, { useContext } from "react";
import { BuahContext } from "./BuahContext";
import axios from "axios";
import "../App.css";

const BuahList = () => {
  const [buah, setBuah, input, setInput] = useContext(BuahContext);

  const handleDelete = (event) => {
    let idBuah = parseInt(event.target.value);
    axios
      .delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
      .then((res) => {
        let newDataBuah = buah.filter((x) => x.id !== idBuah);
        setBuah(newDataBuah);
      });
  };

  const handleEdit = (event) => {
    var idBuah = parseInt(event.target.value);
    var Buah = buah.find((x) => x.id === idBuah);
    setInput({
      ...input,
      name: Buah.name,
      price: Buah.price,
      weight: Buah.weight,
      id: idBuah,
    });
  };

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Daftar Harga Buah</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {buah !== null &&
            buah.map((el, idx) => {
              return (
                <tr key={el.id}>
                  <td>{idx + 1}</td>
                  <td>{el.name}</td>
                  <td>{el.price}</td>
                  <td>{el.weight / 1000} kg</td>
                  <td>
                    <button onClick={handleEdit} value={el.id}>
                      Edit
                    </button>
                    &nbsp;
                    <button onClick={handleDelete} value={el.id}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
};

export default BuahList;
