import React, { useContext } from "react";
import { BuahContext } from "./BuahContext";
import axios from "axios";
import "../App.css";

const BuahForm = () => {
  const [buah, setBuah, input, setInput] = useContext(BuahContext);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (input.id === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/fruits`, {
          name: input.name,
          price: input.price,
          weight: input.weight,
        })
        .then((res) => {
          let data = res.data;
          setBuah([
            ...buah,
            {
              id: data.id,
              name: data.name,
              price: data.price,
              weight: data.weight,
            },
          ]);
          setInput({ id: null, name: "", price: "", weight: 0 });
        });
    } else {
      axios
        .put(
          `http://backendexample.sanbercloud.com/api/fruits/${input.id}`,
          {
            name: input.name,
            price: input.price,
            weight: input.weight,
          }
        )
        .then((res) => {
          let newDataBuah = buah.map((x) => {
            if (x.id === input.id) {
              x.name = input.name;
              x.price = input.price;
              x.weight = input.weight;
            }
            return x;
          });
          setBuah(newDataBuah);
          setInput({ id: null, name: "", price: "", weight: 0 });
        });
    }
  };

  const handleChangeName = (event) => {
    setInput({ ...input, name: event.target.value });
  };

  const handleChangePrice = (event) => {
    setInput({ ...input, price: event.target.value });
  };

  const handleChangeWeight = (event) => {
    setInput({ ...input, weight: event.target.value });
  };

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Form Daftar Harga Buah</h1>
      <div className="border-form">
        <form
          onSubmit={handleSubmit}
          style={{ textAlign: "center", marginTop: "10px" }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "15px",
            }}
          >
            <label>Nama: </label>
            <input type="text" value={input.name} onChange={handleChangeName} />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "15px",
            }}
          >
            <label>Harga: </label>
            <input
              type="text"
              value={input.price}
              onChange={handleChangePrice}
            />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "15px",
            }}
          >
            <label>Berat (dalam gram): </label>
            <input
              type="text"
              value={input.weight}
              onChange={handleChangeWeight}
            />
          </div>
          <div style={{ margin: "15px", textAlign: "right" }}>
            <button>Submit</button>
          </div>
        </form>
      </div>
    </>
  );
};

export default BuahForm;
