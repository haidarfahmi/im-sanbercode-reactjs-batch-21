import React, { useState, createContext, useEffect } from "react";
import axios from "axios";
import "../App.css";

export const BuahContext = createContext();

export const BuahProvider = (props) => {
  const [buah, setBuah] = useState(null);

  const [input, setInput] = useState({
    name: "",
    price: "",
    weight: 0,
    id: null,
  });

  useEffect(() => {
    if (buah === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          setBuah(res.data);
        });
    }
  }, [buah]);

  return (
    <BuahContext.Provider value={[buah, setBuah, input, setInput]}>
      {props.children}
    </BuahContext.Provider>
  );
};
