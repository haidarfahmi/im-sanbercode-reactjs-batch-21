import React, { useState, useEffect } from "react";
import axios from "axios";
import "../App.css";

const FormFruits = () => {
  const [dataFruits, setDataFruits] = useState(null);
  const [input, setInput] = useState({
    name: "",
    price: "",
    weight: 0,
    id: null,
  });

  useEffect(() => {
    if (dataFruits === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          setDataFruits(res.data);
        });
    }
  }, [dataFruits]);

  const submitForm = (event) => {
    event.preventDefault();
    if (input.id === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/fruits`, {
          name: input.name,
          price: input.price,
          weight: input.weight,
        })
        .then((res) => {
          let data = res.data;
          setDataFruits([
            ...dataFruits,
            {
              id: data.id,
              name: data.name,
              price: data.price,
              weight: data.weight,
            },
          ]);
          setInput({ id: null, name: "", price: "", weight: 0 });
        });
    } else {
      axios
        .put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {
          name: input.name,
          price: input.price,
          weight: input.weight,
        })
        .then((res) => {
          let newDataFruits = dataFruits.map((el) => {
            if (el.id === input.id) {
              el.name = input.name;
              el.price = input.price;
              el.weight = input.weight;
            }
            return el;
          });
          setDataFruits(newDataFruits);
          setInput({ id: null, name: "", price: "", weight: 0 });
        });
    }
  };

  const deleteFruits = (event) => {
    let idFruits = parseInt(event.target.value);
    axios
      .delete(`http://backendexample.sanbercloud.com/api/fruits/${idFruits}`)
      .then((res) => {
        let newDataFruits = dataFruits.filter((el) => el.id !== idFruits);
        setDataFruits(newDataFruits);
      });
  };

  const changeInputNama = (event) => {
    let value = event.target.value;
    setInput({ ...input, name: value });
  };
  const changeInputHarga = (event) => {
    let value = event.target.value;
    setInput({ ...input, price: value });
  };
  const changeInputBerat = (event) => {
    let value = event.target.value;
    setInput({ ...input, weight: value });
  };

  const editForm = (event) => {
    let idFruits = parseInt(event.target.value);
    let fruits = dataFruits.find((el) => el.id === idFruits);
    setInput({
      id: idFruits,
      name: fruits.name,
      price: fruits.price,
      weight: fruits.weight,
    });
  };

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Daftar Harga Buah</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {dataFruits !== null &&
            dataFruits.map((item, index) => {
              return (
                <tr key={item.id}>
                  <td>{index + 1}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>{item.weight / 1000} kg</td>
                  <td>
                    <button onClick={editForm} value={item.id}>
                      Edit
                    </button>
                    &nbsp;
                    <button onClick={deleteFruits} value={item.id}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
      {/* FORM */}
      <h1 style={{ textAlign: "center" }}>Form Daftar Harga Buah</h1>
      <div className="border-form">
        <form
          onSubmit={submitForm}
          style={{ textAlign: "center", marginTop: "10px" }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "15px",
            }}
          >
            <label>Nama: </label>
            <input type="text" value={input.name} onChange={changeInputNama} />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "15px",
            }}
          >
            <label>Harga: </label>
            <input
              type="text"
              value={input.price}
              onChange={changeInputHarga}
            />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "15px",
            }}
          >
            <label>Berat (dalam gram): </label>
            <input
              type="text"
              value={input.weight}
              onChange={changeInputBerat}
            />
          </div>
          <div style={{ margin: "15px", textAlign: "right" }}>
            <button>Submit</button>
          </div>
        </form>
      </div>
    </>
  );
};
export default FormFruits;
