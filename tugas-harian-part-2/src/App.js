// import logo from './logo.svg';
// import Tugas9 from './Tugas-9/tugas9';
// import Tugas10 from './Tugas-10/tugas10';
// import Tugas11 from './Tugas-11/tugas11';
// import Tugas12 from './Tugas-12/tugas12';
// import Tugas13 from './Tugas-13/tugas13';
// import Tugas14 from './Tugas-14/Buah'
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Routes from './Tugas-15/Routes';

const App = () => {
    return(
        <div>
        <Router>
            <Routes />
        </Router>
        </div>
    );
};

export default App;