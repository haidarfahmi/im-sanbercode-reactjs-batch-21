import React, { useContext, useState } from "react";
import { ThemeContext, themes } from "./Theme";

const Tugas15 = () => {
  const [tema, setTema] = useContext(ThemeContext);
  const [isLight, setIsLight] = useState(true);

  const klik = () => setIsLight(!isLight);

  const handleTema = () => {
    if (tema === themes.dark) {
      setTema(themes.light);
    } else {
      setTema(themes.dark);
    }
  };

  return (
    <div className="holder">
      <button style={{ padding: "10px" }} onClick={() => { handleTema(); klik();}}>
        {!!isLight ? "Change Navigation to Dark Theme":"Change Navigation to Light Theme"}
      </button>
    </div>
  );
};

export default Tugas15;
