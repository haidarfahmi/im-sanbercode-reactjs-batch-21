import React from 'react';
import '../App.css';

class DaftarBuah extends React.Component {
	render() {
		return (
			<div>
				<input type="checkbox" name={this.props.name} value={this.props.name} />
				{this.props.name}
				<br />
			</div>
		);
	}
}
var buah = [ { name: 'Semangka' }, { name: 'Jeruk' }, { name: 'Nanas' }, { name: 'Salak' }, { name: 'Anggur' } ];

class Tugas9 extends React.Component {
	render() {
		return (
			<div>
				<div className="FormBuah">
					<h1 style={{ textAlign: 'center' }}>Form Pembelian Buah</h1>

					<table style={{ margin: '10px' }}>
						<tr>
							<td>
								<b>Nama Pelanggan</b>
							</td>
							<td />
							<td />
							<td>
								<input type="text" name="name" />
							</td>
						</tr>
						<tr />
						<td style={{ verticalAlign: 'bottom' }}>
							<b>Daftar Item</b>
						</td>
						<td />
						<td />
						<td style={{ verticalAlign: 'bottom' }}>
							{buah.map((el) => {
								return (
									<div>
										<DaftarBuah name={el.name} />
									</div>
								);
							})}
						</td>
						<tr />
						<tr />
						<tr>
							<td>
								<button style={({ background: 'white', borderRadius: '20px', marginTop: '20px' })}>Kirim</button>
							</td>
						</tr>
					</table>
				</div>
			</div>
		);
	}
}

export default Tugas9;
