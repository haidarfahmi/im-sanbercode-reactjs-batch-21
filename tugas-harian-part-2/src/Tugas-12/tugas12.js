import React, { Component } from "react";
import "../App.css";

class ListForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataHargaBuah: [
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 },
      ],
      inputNama: "",
      inputHarga: "",
      inputBerat: 0,
      index: -1,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    let index = this.state.index;
    let tambahData = {
      nama: this.state.inputNama,
      harga: this.state.inputHarga,
      berat: this.state.inputBerat,
    };
    if (index === -1) {
      this.setState({
        dataHargaBuah: [...this.state.dataHargaBuah, tambahData],
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
      });
    } else {
      let newHargaBuah = this.state.dataHargaBuah;
      newHargaBuah[index] = tambahData;
      this.setState({
        dataHargaBuah: [...newHargaBuah],
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
        index: -1,
      });
    }
  }

  changeInputName = (event) => {
    let value = event.target.value;
    this.setState({ inputNama: value });
  };
  changeInputHarga = (event) => {
    let value = event.target.value;
    this.setState({ inputHarga: value });
  };
  changeInputBerat = (event) => {
    let value = event.target.value;
    this.setState({ inputBerat: value });
  };

  handleEdit = (event) => {
    let index = event.target.value;
    let hargaBuah = this.state.dataHargaBuah[index];
    this.setState({
      inputNama: hargaBuah.nama,
      inputHarga: hargaBuah.harga,
      inputBerat: hargaBuah.berat,
      index: index,
    });
  };

  handleDelete(event) {
    let index = event.target.value;
    let newHargaBuah = this.state.dataHargaBuah;
    newHargaBuah.splice(index, 1);
    this.setState({
      daftarBuah: [...newHargaBuah],
      inputNama: "",
      inputHarga: "",
      inputBerat: "",
      index: -1,
    });
  }

  render() {
    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Tabel Harga Buah</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {this.state.dataHargaBuah.map((props, index) => {
              return (
                <tr>
                  <td>{index+1}</td>
                  <td>{props.nama}</td>
                  <td>{props.harga}</td>
                  <td>{props.berat / 1000} kg</td>
                  <td>
                    <button
                      value={index}
                      onClick={this.handleEdit}
                      style={{ marginRight: "10px" }}
                    >
                      Edit
                    </button>
                    <button value={index} onClick={this.handleDelete}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {/* FORM */}
        <h1 style={{ textAlign: "center" }}>Form Input Buah</h1>
        <div className="border-form">
          <form
            onSubmit={this.handleSubmit}
            style={{ textAlign: "center", marginTop: "10px" }}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                margin: "15px",
              }}
            >
              <label>Nama: </label>
              <input
                type="text"
                value={this.state.inputNama}
                onChange={this.changeInputName}
              />
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                margin: "15px",
              }}
            >
              <label>Harga: </label>
              <input
                type="text"
                value={this.state.inputHarga}
                onChange={this.changeInputHarga}
              />
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                margin: "15px",
              }}
            >
              <label>Berat (dalam gram): </label>
              <input
                type="text"
                value={this.state.inputBerat}
                onChange={this.changeInputBerat}
              />
            </div>
            <div style={{ margin: "15px", textAlign: "right" }}>
              <button>Submit</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default ListForm;
