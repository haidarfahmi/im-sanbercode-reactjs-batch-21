import React from "react";
import "../App.css";

const About = () => {
  return (
    <div className="container">
      <div className="border-about">
        <h1 style={{ textAlign: "center" }}>
          Data Peserta Sanbercode Bootcamp ReactJS
        </h1>
        <ol>
          <li>
            <strong width="100px">Nama:</strong> Muhammad Haidar Fahmi
          </li>
          <li>
            <strong width="100px">Email:</strong> haidarfahmi2000@gmail.com
          </li>
          <li>
            <strong width="100px">Sistem Operasi yang digunakan:</strong>{" "}
            Windows 10
          </li>
          <li>
            <strong width="100px">Akun Gitlab:</strong> @haidarfahmi
          </li>
          <li>
            <strong width="100px">Akun Telegram:</strong> @haidarfahmi
          </li>
        </ol>
      </div>
    </div>
  );
};

export default About;
