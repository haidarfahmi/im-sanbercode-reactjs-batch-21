import React, { Component } from "react";
import axios from "axios";
import "./home.css";

class Books extends Component {
  constructor(props) {
    super(props);
    this.state = {
      daftarBooks: [],
      title: "",
      description: "",
      review: "",
      release_year: "",
      totalPage: "",
      price: "",
      image_url: "",
    };
  }

  componentDidMount() {
    axios.get(`http://backendexample.sanbercloud.com/api/books`).then((res) => {
      const books = res.data;
      this.setState({
        daftarBooks: books,
      });
    });
  }

  render() {
    console.log(this.state.daftarBooks);
    return (
        <div className="container">
          <h1 style={{ textAlign: "center" }}>Daftar Buku-Buku Pilihan</h1>
          {this.state.daftarBooks.map((val, index) => {
            return (
              <div key={index} className="books-list">
                <h2 style={{ float: "left" }}>{val.title}</h2>
                <div className="top">
                  <img src={val.image_url} alt="poster-book" />
                  <div className="ket">
                    <p>
                      <b>Tahun Terbit : {val.release_year} </b>
                    </p>
                    <p>
                      <b>Harga : Rp. {val.price} </b>
                    </p>
                    <p>
                      <b>Jumalah Halaman : {val.totalPage} </b>
                    </p>
                  </div>
                </div>
                <div className="bot">
                  <p>Deskripsi : {val.description}</p>
                  <p>Ulasan : {val.review}</p>
                </div>
              </div>
            );
          })}
        </div>
    );
  }
}

export default Books;
