import React from "react";
import "./App.css";
import Routes from "./Router/router";
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <>
      <Router>
        <Routes />
      </Router>
      <footer>
        <h5>copyright &copy; 2021 by Sanbercode</h5>
      </footer>
    </>
  );
}

export default App;
