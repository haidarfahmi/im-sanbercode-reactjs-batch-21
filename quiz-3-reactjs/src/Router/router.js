import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../Home/home'
import About from '../About/about';
import Books from '../Books/Book';
import Login from '../Login/login';
import Nav from './nav';

const App = () => {
    return (
        <>
            <Nav />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/about" component={About} />
                <Route exact path="/booklist" component={Books} />
                <Route exact path="/login" component={Login} />
            </Switch>
        </>
    );
};

export default App;
