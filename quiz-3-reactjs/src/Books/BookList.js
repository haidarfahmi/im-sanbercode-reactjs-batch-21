import React, { useState, useEffect, useContext } from "react";
import { BooksContext } from "./BookContext";
import axios from "axios";

const BookList = () => {
  const [books, setBooks, input, setInput] = useContext(BooksContext);
  const [cari, setCari] = useState('');
  const [filterBuku, setFilterBuku] = useState(null);

  useEffect(() => {
    if (books !== null) {
      setFilterBuku(
        books.filter((item) => {
          return item.title.toLowerCase().includes(cari.toLocaleLowerCase());
        })
      );
    }
  }, [cari, books]);

  const hapusBuku = (event) => {
    let idBooks = parseInt(event.target.value);

    let newBooks = books.filter((el) => el.id !== idBooks);

    axios
      .delete(`http://backendexample.sanbercloud.com/api/books/${idBooks}`)
      .then((res) => {
        console.log(res);
      });

    setBooks([...newBooks]);
  };

  const ubahBuku = (event) => {
    let idBooks = parseInt(event.target.value);
    let newBooks = books.find((x) => x.id === idBooks);
    setInput({
      title: newBooks.title,
      description: newBooks.description,
      review: newBooks.review,
      release_year: newBooks.release_year,
      totalPage: newBooks.totalPage,
      price: newBooks.price,
      image_url: newBooks.image_url,
      id: newBooks.id,
    });
  };

  const updateSearch = (event) => {
    setCari(event.target.value.substr(0, 20));
  };

  return (
    <div className="container">
      <h1>Daftar Buku</h1>
      <div style={{ width: "90%" }}>
        <input
          type="text"
          placeholder="Cari Buku"
          style={{ float: "right" }}
          value={cari}
          onChange={updateSearch}
        />
      </div>

      <table>
          <thead>
              <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Review</th>
                  <th>Release Year</th>
                  <th>Total Page</th>
                  <th>Price</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              {filterBuku !== null &&
                filterBuku.map((item, index) => {
                    return(
                        <tr key={item.id}>
                            <td>{index+1}</td>
                            <td>{item.title}</td>
                            <td>{item.description}</td>
                            <td>{item.review}</td>
                            <td>{item.release_year}</td>
                            <td>{item.totalPage}</td>
                            <td>{item.price}</td>
                            <td>
                                <button onClick={ubahBuku} value={item.id}>Edit</button>
                                &nbsp;
                                <button onClick={hapusBuku} value={item.id}>Delete</button>
                            </td>
                        </tr>
                    )
                })
              }
          </tbody>
      </table>
    </div>
  );
};

export default BookList;
