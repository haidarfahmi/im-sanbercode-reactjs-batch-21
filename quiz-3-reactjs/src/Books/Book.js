import React from "react";
import { BooksProvider } from "./BookContext";
import BookList from "./BookList";
import BookForm from "./BookForm";
import "./book.css";

const Book = () => {
  return (
    <BooksProvider>
      <BookList />
      <BookForm />
    </BooksProvider>
  );
};

export default Book;
