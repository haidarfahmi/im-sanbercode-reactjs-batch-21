import React, { useState, useEffect, createContext } from 'react'
import axios from 'axios'

export const BooksContext = createContext()

export const BooksProvider = (props) =>{
    const [ books, setBooks ] = useState(null)
    const [ input, setInput ] = useState({
        title: '',
        description: '',
        review: '',
        release_year: 2020,
        totalPage: 0,
        price: 0,
        image_url: '',
        id: null,
    })

    useEffect(() => {
        if (books === null) {
            axios
                .get(`http://backendexample.sanbercloud.com/api/books`)
                .then((res) => {
                setBooks(
                    res.data.map((el) => {
                        return {
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            review: el.review,
                            release_year: el.release_year,
                            totalPage: el.totalPage,
                            price: el.price,
                            image_url: el.image_url,
                        }
                    })
                )
            })
        }
    }, [books])
    
    return(
        <BooksContext.Provider value={[books, setBooks, input, setInput]}>
            {props.children}
        </BooksContext.Provider>
    )
}