import React, { useContext } from "react";
import { BooksContext } from "./BookContext";
import axios from "axios";

const BookForm = () => {
  const [books, setBooks, input, setInput] = useContext(BooksContext);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (input.id === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/books`, {
          title: input.title,
          description: input.description,
          review: input.review,
          release_year: input.release_year,
          totalPage: input.totalPage,
          price: input.price,
          image_url: input.image_url,
        })
        .then((res) => {
          setBooks([
            ...books,
            {
              id: res.data.id,
              title: res.data.title,
              description: res.data.description,
              review: res.data.review,
              release_year: res.data.release_year,
              totalPage: res.data.totalPage,
              price: res.data.price,
              image_url: res.data.image_url,
            },
          ]);
          setInput({
            id: null,
            title: "",
            description: "",
            review: "",
            release_year: 0,
            totalPage: 0,
            price: 0,
            image_url: "",
          });
        });
    } else {
      axios
        .put(`http://backendexample.sanbercloud.com/api/books/${input.id}`, {
          title: input.title,
          description: input.description,
          review: input.review,
          release_year: input.release_year,
          totalPage: input.totalPage,
          price: input.price,
          image_url: input.image_url,
        })
        .then(() => {
          let newDataBooks = books.map((x) => {
            if (x.id === input.id) {
              x.title = input.title;
              x.description = input.description;
              x.review = input.review;
              x.release_year = input.release_year;
              x.totalPage = input.totalPage;
              x.price = input.price;
              x.image_url = input.image_url;
            }
            return x;
          });
          setBooks(newDataBooks);
          setInput({
            title: "",
            description: "",
            review: "",
            release_year: 2020,
            totalPage: 0,
            price: 0,
            image_url: "",
            id: null,
          });
        });
    }
  };

  const handleChange = (event) => {
    let typeOfInput = event.target.name;

    switch (typeOfInput) {
      case "title": {
        setInput({ ...input, title: event.target.value });
        break;
      }
      case "description": {
        setInput({ ...input, description: event.target.value });
        break;
      }
      case "review": {
        setInput({ ...input, review: event.target.value });
        break;
      }
      case "release_year": {
        setInput({ ...input, release_year: event.target.value });
        break;
      }
      case "totalPage": {
        setInput({ ...input, totalPage: event.target.value });
        break;
      }
      case "price": {
        setInput({ ...input, price: event.target.value });
        break;
      }
      case "image_url": {
        setInput({ ...input, image_url: event.target.value });
        break;
      }
      default: {
        break;
      }
    }
  };

  return (
    <div className="container">
      <h1>Books Form</h1>
      <div style={{ width: "70%", margin: "0 auto", display: "block" }}>
        <div style={{ border: "1px solid black", padding: "20px" }}>
          <form onSubmit={handleSubmit}>
            <div className="label-form">
              <label style={{ float: "left" }}>
                <b>Title:</b>
              </label>
              <input
                style={{ float: "left", marginLeft: "85px", width: 220 }}
                type="text"
                required
                name="title"
                value={input.title}
                onChange={handleChange}
              />
            </div>
            <div className="label-form">
              <label style={{ float: "left" }}>
                <b>Description:</b>
              </label>
              <textarea
                style={{
                  float: "left",
                  marginLeft: "30px",
                  width: 400,
                  height: 80,
                }}
                type="text"
                required
                name="description"
                value={input.description}
                onChange={handleChange}
              />
            </div>
            <div className="label-form">
              <label style={{ float: "left" }}>
                <b>Review:</b>
              </label>
              <textarea
                style={{
                  float: "left",
                  marginLeft: "60px",
                  width: 400,
                  height: 80,
                }}
                type="text"
                required
                name="review"
                value={input.review}
                onChange={handleChange}
              />
            </div>
            <div className="label-form">
              <label style={{ float: "left" }}>
                <b>Release Year:</b>
              </label>
              <input
                min={1980}
                style={{ float: "left", marginLeft: "20px", width: 100 }}
                type="number"
                required
                name="release_year"
                value={input.release_year}
                onChange={handleChange}
              />
            </div>
            <div className="label-form">
              <label style={{ float: "left" }}>
                <b>Total Page:</b>
              </label>
              <input
                style={{ float: "left", marginLeft: "35px", width: 220 }}
                type="number"
                required
                name="totalPage"
                value={input.totalPage}
                onChange={handleChange}
              />
            </div>
            <div className="label-form">
              <label style={{ float: "left" }}>
                <b>Price:</b>
              </label>
              <input
                style={{ float: "left", marginLeft: "75px", width: 220 }}
                type="number"
                required
                name="price"
                value={input.price}
                onChange={handleChange}
              />
            </div>
            <div className="label-form">
              <label style={{ float: "left" }}>
                <b>Image URL:</b>
              </label>
              <textarea
                style={{
                  float: "left",
                  marginLeft: "30px",
                  width: 400,
                  height: 80,
                }}
                type="text"
                required
                name="image_url"
                value={input.image_url}
                onChange={handleChange}
              />
            </div>
            <div style={{ width: "100%", paddingBottom: "20px" }}>
              <button style={{ float: "left" }}>Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default BookForm;
