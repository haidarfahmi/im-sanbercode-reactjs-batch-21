// * SOAL 1 - Increment & Decrement whlile
//! Increment
console.log('SOAL 1 - LOOPING WHILE');
var i = 2;
console.log('LOOPING 1 - INCREMENT');
while (i <= 20) {
	console.log(i + ' - i love coding');
	i += 2;
}
// ! Decrement
var i = 20;
console.log('LOOPING 2 - DECREMENT');
while (i >= 2) {
	console.log(i + ' - i love coding');
	i -= 2;
}

// * SOAL 2 - Looping for
//! Syarat
// 1. Jika angka ganjil maka tampilkan Santai
// 2. Jika angka genap maka tampilkan Berkualitas
// 3. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
console.log('SOAL 2 - LOOPING FOR');
for (var i = 1; i <= 20; i++) {
	if (i % 2 == 0) {
		console.log(i + ' - Berkualitas');
	} else if (i % 3 == 0 && i % 2 == 1) {
		console.log(i + ' - I Love Coding');
	} else {
		console.log(i + ' - Santai');
	}
}

// * SOAL 3 - Looping membuat segitiga
console.log('SOAL 3 - Segitiga');
var hasil = '';
for (var a = 1; a <= 7; a++) {
	for (var b = 1; b <= a; b++) {
		hasil += '#';
	}
	hasil += '\n';
}
console.log(hasil);

// * SOAL 4 - Array
console.log('SOAL 4 - Array');
var kalimat = 'saya sangat senang belajar javascript';
var split = kalimat.split(' ');
console.log(split);

//* SOAL 5 - Array + Loop
console.log('SOAL 5 - Array + Loop');
var daftarBuah = [ '2. Apel', '5. Jeruk', '3. Anggur', '4. Semangka', '1. Mangga' ];
var buah = daftarBuah.sort();
for (var i = 0; i < buah.length; i++) {
	console.log(buah[i]);
}
