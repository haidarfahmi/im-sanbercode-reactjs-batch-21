// * SOAL 1 - Membuat function
console.log('---- SOAL 1 ----');

function halo() {
	return 'Halo Sanbers!';
}
console.log(halo());

//* SOAL 2 - Membuat function
console.log('---- SOAL 2 ----');

function kalikan(angkaPertama, angkaKedua) {
	return angkaPertama * angkaKedua;
}
var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

// * SOAL 3
console.log('---- SOAL 3 ----');

function introduce(a, b, c, d) {
	var kalimat =
		'Nama saya ' +
		a +
		', umur saya ' +
		b +
		' tahun, alamat saya di ' +
		c +
		', dan saya punya hobby yaitu ' +
		d +
		'!';
	return kalimat;
}
var name = 'John';
var age = 30;
var address = 'Jalan belum jadi';
var hobby = 'Gaming';

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"

// * SOAL 4 - Mengubah Array menjadi Object
console.log('---- SOAL 4 ----');
var arrayDaftarPeserta = [ 'Asep', 'laki-laki', 'baca buku', 1992 ];
var obj = {
	nama: arrayDaftarPeserta[0],
	jnskelamin: arrayDaftarPeserta[1],
	hobi: arrayDaftarPeserta[2],
	tahun: arrayDaftarPeserta[3]
};
// Output array
console.log('Ouput dalam bentuk array : ');
console.log(arrayDaftarPeserta);
// Output nama dalam object
console.log('Ouput nama dalam Object : ');
console.log(obj.nama);
// Output object
console.log('Ouput dalam bentuk Object : ');
console.log(obj);

// * SOAL 5
console.log('---- SOAL 5 ----');
// ! Data
// 1.nama: strawberry
//   warna: merah
//   ada bijinya: tidak
//   harga: 9000
// 2.nama: jeruk
//   warna: oranye
//   ada bijinya: ada
//   harga: 8000
// 3.nama: Semangka
//   warna: Hijau & Merah
//   ada bijinya: ada
//   harga: 10000
// 4.nama: Pisang
//   warna: Kuning
//   ada bijinya: tidak
//   harga: 5000
var buah = [
	{ nama: 'strawberry', warna: 'merah', adaBiji: false, harga: 9000 },
	{ nama: 'jeruk', warna: 'oranye', adaBiji: true, harga: 8000 },
	{ nama: 'Semangka', warna: 'Hijau & Merah', adaBiji: true, harga: 10000 },
	{ nama: 'Pisang', warna: 'Kuning', adaBiji: false, harga: 5000 }
];
console.log(buah[0]);

// * SOAL 6
console.log('---- SOAL 6 ----');

var dataFilm = [];
function tambahData(name, duration, genres, years) {
	var film = {
		nama: name,
		durasi: duration,
		genre: genres,
		tahun: years
	};
	dataFilm.push(film);
	return dataFilm;
}
tambahData('Ready Player One', '2 jam', 'Adventure', 2018);
tambahData('Aquaman', '2 jam', 'Action', 2018);
tambahData('Jumanji: The Next Level', '2 jam', 'Comedy', 2019);
console.log(dataFilm);
