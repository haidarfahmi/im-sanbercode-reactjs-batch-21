var readBooksPromise = require('./promise.js');

var books = [
	{ name: 'LOTR', timeSpent: 3000 },
	{ name: 'Fidas', timeSpent: 2000 },
	{ name: 'Kalkulus', timeSpent: 4000 }
];

// Lanjutkan code untuk menjalankan function readBooksPromise
function bacaBuku(time, x) {
	readBooksPromise(time, books[x])
		.then(function(fulfilled) {
			if (x < books.length) {
				bacaBuku(fulfilled, x + 1);
			}
		})
		.catch(function(reject) {
			if (x > books.length) {
				console.log(reject);
			}
		});
}
bacaBuku(10000, 0);
