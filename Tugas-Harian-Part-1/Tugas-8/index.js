// di index.js
// function readBooks menerima input waktu yang dimiliki yaitu 10000 ms (10 detik) dan books pada indeks ke-0.
// Setelah mendapatkan callback sisa waktu yang dikirim lewat callback, sisa waktu tersebut dipakai untuk membaca buku pada indeks ke-1.
// Begitu seterusnya sampai waktu habis atau semua buku sudah terbaca

var readBooks = require('./callback.js');

var books = [
	{ name: 'LOTR', timeSpent: 3000 },
	{ name: 'Fidas', timeSpent: 2000 },
	{ name: 'Kalkulus', timeSpent: 4000 },
	{ name: 'komik', timeSpent: 1000 }
];

// Tulis code untuk memanggil function readBooks di sini
function membaca(time, x) {
	readBooks(time, books[x], function(sisa) {
		if (sisa >= 0) {
			if (x + 1 < books.length) {
				membaca(sisa, x + 1);
			}
		}
	});
}
membaca(10000, 0);
