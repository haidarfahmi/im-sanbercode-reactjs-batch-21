//* SOAL 1 - Arrwo Function
console.log('--- SOAL 1 ---');
const luasLingkaran = (r) => {
	let result = 3.14 * r * r;
	return result;
};
const kelLingkaran = (r) => {
	let result = 2 * 3.14 * r;
	return result;
};
console.log('Luas Lingkaran : ', luasLingkaran(5));
console.log('Keliling Lingkaran : ', kelLingkaran(5));

//* SOAL 2 - Template Literals
console.log('--- SOAL 2 ---');
let kalimat = '';

const tambahKata = (kata) => {
	kalimat += `${kata} `;
	return kalimat;
};
tambahKata('saya');
tambahKata('adalah');
tambahKata('seorang');
tambahKata('frontend');
tambahKata('developer');
console.log(kalimat);

//* SOAL 3 - Object Literal
console.log('--- SOAL 3 ---');
//! Object Literal ES5
// const newFunction = function literal(firstName, lastName) {
// 	return {
// 		firstName: firstName,
// 		lastName: lastName,
// 		fullName: function() {
// 			console.log(firstName + ' ' + lastName);
// 		}
// 	};
// };
// newFunction('William', 'Imoh').fullName();

//!Object Literal ES6
const newFunction = (firstName, lastName) => {
	return {
		firstName,
		lastName,
		fullName: () => {
			console.log(`${firstName} ${lastName}`);
			return;
		}
	};
};
newFunction('William', 'Imoh').fullName();

//* SOAL 4 - Destructuring
console.log('--- SOAL 4 ---');
const newObject = {
	firstName: 'Harry',
	lastName: 'Potter Holt',
	destination: 'Hogwarts React Conf',
	occupation: 'Deve-wizard Avocado',
	spell: 'Vimulus Renderus!!!'
};
// const firstName = newObject.firstName;   --> ES5
const { firstName, lastName, destination, occupation, spell } = newObject; // --> ES6
console.log(firstName, lastName, destination, occupation, spell);

//* SOAL 5 - Spread Operator
console.log('--- SOAL 5 ---');
const west = [ 'Will', 'Chris', 'Sam', 'Holly' ];
const east = [ 'Gill', 'Brian', 'Noel', 'Maggie' ];
// const combined = west.concat(east); --> ES5
const combined = [ ...west, ...east ]; // --> ES6
console.log(combined);
