// * SOAL 1 - Menggabungkan String
console.log('SOAL 1');
var kataPertama = 'saya';
var kataKedua = 'senang';
var kataKetiga = 'belajar';
var kataKeempat = 'javascript';
// ! Hasil yang diinginkan ==> saya Senang belajar JAVASCRIPT
console.log(
	kataPertama
		.concat(' ')
		.concat(kataKedua.charAt(0).toUpperCase() + kataKedua.substr(1).toLowerCase())
		.concat(' ')
		.concat(kataKetiga)
		.concat(' ')
		.concat(kataKeempat.toUpperCase())
);

//* SOAL 2 - Mengubah Variabel String ke Integer, jumlahkan dan tampilkan output
console.log('SOAL 2');
var katasatu = '1';
var katadua = '2';
var katatiga = '4';
var kataempat = '5';
// ! Menampilkan Tipe data sebelum
a = typeof katasatu;
console.log('Tipe data kataPertama : ' + a);
b = typeof katadua;
console.log('Tipe data kataKedua : ' + b);
c = typeof katatiga;
console.log('Tipe data kataKetiga : ' + c);
d = typeof kataempat;
console.log('Tipe data kataKeempat : ' + d);
console.log('Hasil penjumlahan sebelum :', katasatu + katadua + katatiga + kataempat); //12345
// ! Mengubah Tipe data string ke integer
var Int1 = Number(katasatu);
var Int2 = Number(katadua);
var Int3 = Number(katatiga);
var Int4 = Number(kataempat);
// ! Meampilkan Tipe data sesudah
f = typeof Int1;
console.log('Tipe data kataPertama : ' + f);
g = typeof Int2;
console.log('Tipe data kataKedua : ' + g);
h = typeof Int3;
console.log('Tipe data kataKetiga : ' + h);
i = typeof Int4;
console.log('Tipe data kataKeempat : ' + i);
console.log('Hasil penjumlahan sesudah : ', Int1 + Int2 + Int3 + Int4); //12

// * SOAL 3
console.log('SOAL 3');
// ! Output yang diinginkan
// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali
var kalimat = 'wah javascript itu keren sekali';

var kata1 = kalimat.substring(0, 3); //Cara dengan menggunakan .substring([index awal], [index akhir]) / .substring(start, end)
var kata2 = kalimat.substr(4, 10); //Cara dengan mengunakan .substr([index awal], [jumlah karakter]) / .substr(start, lenght)
var kata3 = kalimat.substring(15, 18);
var kata4 = kalimat.substr(19, 5);
var kata5 = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kata1);
console.log('Kata Kedua: ' + kata2);
console.log('Kata Ketiga: ' + kata3);
console.log('Kata Keempat: ' + kata4);
console.log('Kata Kelima: ' + kata5);

// * SOAL 4 - Pengkondisian Variabel Nilai (IF-ELSE)
// ! Output yang diinginkan
// nilai >= 80 indeksnya A
// nilai >= 70 dan nilai < 80 indeksnya B
// nilai >= 60 dan nilai < 70 indeksnya c
// nilai >= 50 dan nilai < 60 indeksnya D
// nilai < 50 indeksnya E
console.log('SOAL 4');
var nilai = 90; //Silahkan masukan nilai bebas di varibel nilai

if (nilai >= 80) {
	console.log('Indeks Nilai A');
} else if (nilai >= 70 && nilai < 80) {
	console.log('Indeks Nilai B');
} else if (nilai >= 60 && nilai < 70) {
	console.log('Indeks Nilai C');
} else if (nilai >= 50 && nilai < 60) {
	console.log('Indeks Nilai D');
} else {
	console.log('Indeks Nilai E');
}

// * SOAL 5
console.log('SOAL 5');
var tanggal = 18;
var bulan = 9;
var tahun = 2000;

switch (bulan) {
	case 1:
		a = 'Januari';
		break;
	case 2:
		a = 'Februari';
		break;
	case 3:
		a = 'Maret';
		break;
	case 4:
		a = 'April';
		break;
	case 5:
		a = 'Mei';
		break;
	case 6:
		a = 'Juni';
		break;
	case 7:
		a = 'Juli';
		break;
	case 8:
		a = 'Agustus';
		break;
	case 9:
		a = 'September';
		break;
	case 10:
		a = 'Oktober';
		break;
	case 11:
		a = 'November';
		break;
	case 12:
		a = 'Desember';
		break;
	default:
		a = 'Undifiend';
		break;
}

console.log(tanggal, a, tahun);
